#!/bin/sh

set -e

export flextesa_commit=d33f98de

export flextesa_build_image=registry.gitlab.com/tezos/flextesa:$flextesa_commit-build

smartpy_version="$(./please.sh get_smartpy_recommended_version)"

dockerfile_setup () {
    cat <<EOF
FROM $flextesa_build_image
RUN sudo apk add net-tools jq rlwrap parallel nodejs npm python3
RUN curl -s https://SmartPy.io/${smartpy_version}/cli/SmartPy.sh -o /tmp/SmartPy.sh
RUN sh /tmp/SmartPy.sh local-install-auto /tmp/install-smpy
WORKDIR /usr/bin
RUN sudo sh -c "mv /tmp/install-smpy/* ."
RUN opam pin add -n angstrom 0.13.0
RUN opam install -y base fmt uri cmdliner ezjsonm ocamlformat uri merlin ppx_deriving angstrom
EOF
}
# /src ./dune-project ./please.sh ./multi_asset.py
dockerfile_build () {
    cat <<EOF
FROM $1
RUN sudo mkdir -p /buildfa2
RUN sudo chown -R opam:opam /buildfa2
WORKDIR /buildfa2
ADD --chown=opam:opam  ./dune-project ./please.sh ./multi_asset.py ./
ADD --chown=opam:opam  ./src ./src
RUN find .
RUN ./please.sh build
RUN sudo ./please.sh install
EOF
}
binaries="
flextesa
tezos-node
tezos-client
tezos-admin-client
tezos-validator
tezos-signer
tezos-codec
tezos-protocol-compiler
tezos-baker-alpha
tezos-endorser-alpha
tezos-accuser-alpha
fatoo
"
dockerfile_run () {
    cat <<EOF
FROM $1
FROM alpine
# Stolen from Flextesa:
RUN sh -c "echo '@testing http://nl.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories"
RUN sh -c "echo '@testing http://nl.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories"
RUN apk update
RUN apk add curl net-tools rlwrap@testing gmp-dev hidapi-dev@testing m4 perl pkgconfig libev-dev
EOF
    for b in $binaries ; do
        echo "COPY --from=0 /usr/bin/$b /usr/bin/$b"
    done
}

build () {
    "$@" > Dockerfile
    cat Dockerfile
    docker build -t "fa2:$1" .
}

quicktest () {
    case "$1" in 
        "setup" )
            docker run -v "$PWD:/work" --rm fa2:dockerfile_setup sh -c 'cd /work ; ./please.sh check'
            ;;
        "build" | "run" )
            docker run -v "$PWD:/work" --rm "fa2:dockerfile_$1" sh -c 'cd /work ; fatoo --help=plain'
            ;;
        * )
            echo WUT ; exit 2 ;;
    esac
}

shell (){
    docker run -v "$PWD:/work" --rm -it fa2:test sh
}

"$@"
