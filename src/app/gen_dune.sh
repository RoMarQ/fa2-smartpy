#!/bin/sh

set -e

export version_string="version_20200910_tzip_93e5415e"

export names=$(mktemp -t names-XXX.txt)

open_setenvs() {
    cat <<EOF
(setenv only_environment_test "true"
(setenv debug_mode "$dbg"
(setenv carthage_pairs "$capa"
(setenv force_layouts "$layouts"
(setenv support_operator "$ops"
(setenv assume_consecutive_token_ids "$toknat"
(setenv add_mutez_transfer "$mutran"
(setenv single_asset "$single"
(setenv non_fungible "$nft"
(setenv add_permissions_descriptor "$perdesc"
(setenv lazy_entry_points "$lzep"
(setenv lazy_entry_points_multiple "$lzepm"
EOF
}
close_setenvs () {
    echo "))))))))))))"
}

export admin_address_placeholder=tz1L1bypLzuxGHmx3d6bHFJ2WCi4ZDbocSCA

rule () {
    name="contract"
    description="$1"
    if $dbg ; then name="dbg_$name" ; fi
    if ! $capa ; then name="baby_$name" ; fi
    if ! $layouts ; then name="nolay_$name" ; fi
    if ! $ops ; then name="noops_$name" ; fi
    if ! $toknat ; then name="tokset_$name" ; fi
    if $mutran ; then name="mutran_$name" ; fi
    if $single ; then name="single_$name" ; fi
    if $nft ; then name="nft_$name" ; fi
    if $perdesc ; then name="perdesc_$name" ; fi
    if $lzep ; then name="lzep_$name" ; fi
    if $lzepm ; then name="lzepm_$name" ; fi
    echo "$name" >> "$names"
    all_tokens_literal="[]"
    if $toknat ; then
        all_tokens_literal="M_nat.(Int 0)"
    fi
    empty_big_map="[]"
    if ! $dbg ; then
        empty_big_map="M_big_map.(List [])"
    fi
    metadata_string="${version_string}_$name"
    cat <<EOF
;; Name: $name
;; dbg    : $dbg
;; capa   : $capa
;; layout : $layouts
;; ops    : $ops
;; toknat : $toknat
;; mutran : $mutran
;; single : $single
;; nft    : $nft
;; perdec : $perdesc
;; lzep   : $lzep
;; lzepm  : $lzepm

(rule
  (target fa2_code_$name.ml)
  (deps fa2_$name.tz fa2_init_$name.tz)
  (action
    (with-stdout-to %{target}
      (progn
       (echo "let code = {letshopenobodyusesthisstring|")
       (run cat fa2_$name.tz)
       (echo "|letshopenobodyusesthisstring}")
       (echo "let init ~admin = Base.String.substr_replace_all
 ~pattern:\"$admin_address_placeholder\" ~with_:admin
 {letshopenobodyusesthisstring|")
       (run cat fa2_init_$name.tz)
       (echo "|letshopenobodyusesthisstring}")
))))

(rule
  (target fa2_contract_$name.ml)
  (action
    (with-stdout-to %{target}
     (progn
       (echo "let name = \"$name\"\n")
       (echo "let metadata_string = \"${metadata_string}\"\n")
       (echo "let description = \"$description\"\n")
       (echo "let switches = Sigs.Switches.{\n")
       (echo "debug = $dbg\n")
       (echo "; carthage_pairs = $capa\n")
       (echo "; force_layouts = $layouts\n")
       (echo "; support_operator = $ops\n")
       (echo "; assume_consecutive_token_ids = $toknat\n")
       (echo "; add_mutez_transfer = $mutran\n")
       (echo "; single_asset = $single\n")
       (echo "; non_fungible = $nft\n")
       (echo "; add_permissions_descriptor = $perdesc\n")
       (echo "; lazy_entry_points = $lzep\n")
       (echo "; lazy_entry_points_multiple = $lzepm\n")
       (echo "}\n")
       (echo "let code = Fa2_code_$name.code\n")
       (echo "let init = Fa2_code_$name.init\n")
     )
)))

(rule
  (targets fa2_$name.tz _fa2_intermediate_$name.tz fa2_init_$name.tz)
  (deps multi_asset.py fa2_contract_$name.ml)
  (action
$(open_setenvs)
      (progn
        (run SmartPy.sh compile-smartpy-class multi_asset.py
             "FA2(environment_config(),sp.address(\"$admin_address_placeholder\"))"
             _fa2_$name)
        (run cp  _fa2_$name/multi_asset_compiled.tz _fa2_intermediate_$name.tz)
        (run cp  _fa2_$name/multi_asset_storage_init.tz fa2_init_$name.tz)
        (run flextesa transform
             --replace-annot %self_:%self,%metadata_string:%${metadata_string}
             _fa2_$name/multi_asset_compiled.tz fa2_$name.tz)
        ;;(run flextesa transform
        ;;     --replace-annot %self_:%self,%metadata_string:%${metadata_string}
        ;;     _fa2_$name/multi_asset_storage_init.tz fa2_init_$name.tz)
      )
$(close_setenvs)
))

(rule
   (target fa2_tmp_$name.ml)
   (deps fa2_$name.tz)
   (action (progn
     (run flextesa ocaml --integer-types "bigint" %{deps} %{target})
)))
(rule
   (target fa2_interface_$name.ml)
   (deps fa2_tmp_$name.ml)
   (action
     (with-stdout-to %{target}
       (progn
        (cat fa2_tmp_$name.ml)
        ;;(echo "\n\nlet storage_of_address addr = Storage.make \
        ;;       ~${metadata_string}:M_unit.Unit \
        ;;       ~administrator:M_address.(Raw_b58 addr) \
        ;;       ~all_tokens:$all_tokens_literal \
        ;;       ~ledger:$empty_big_map ~paused:M_bool.False  \
        ;;       ~operators:$empty_big_map  \
        ;;       ~tokens:$empty_big_map\n\n")
       )
     )
   )
)

(rule
  (target fa2_$name-simulation-log.txt)
  (deps multi_asset.py fa2_contract_$name.ml)
  (action
$(open_setenvs)
    (progn
       (run SmartPy.sh test multi_asset.py _fa2_simulation_$name)
       (with-stdout-to %{target} (echo "Test $name"))
       (run sh -c "mv _fa2_simulation_$name/FA2*_interpreted/scenario-interpreter-log.txt %{target}" )
    )
$(close_setenvs)
))
EOF
}

f=false
t=true
dbg=$f capa=$t layouts=$t ops=$t toknat=$t mutran=$f single=$f nft=$f perdesc=$f lzep=$f lzepm=$f rule 'The default'
dbg=$t capa=$t layouts=$t ops=$t toknat=$t mutran=$f single=$f nft=$f perdesc=$f lzep=$f lzepm=$f rule 'The default in debug mode'
dbg=$f capa=$f layouts=$t ops=$t toknat=$t mutran=$f single=$f nft=$f perdesc=$f lzep=$f lzepm=$f rule 'The default in Babylon mode'
dbg=$f capa=$t layouts=$f ops=$t toknat=$t mutran=$f single=$f nft=$f perdesc=$f lzep=$f lzepm=$f rule 'The default without right-combs'
dbg=$f capa=$t layouts=$t ops=$t toknat=$t mutran=$t single=$f nft=$f perdesc=$f lzep=$f lzepm=$f rule 'The default with mutez transfer entry-point'
dbg=$f capa=$t layouts=$t ops=$t toknat=$f mutran=$f single=$f nft=$f perdesc=$f lzep=$f lzepm=$f rule 'The default with non-consecutive token-IDs'
dbg=$f capa=$t layouts=$t ops=$f toknat=$t mutran=$f single=$f nft=$f perdesc=$t lzep=$f lzepm=$f rule 'The default without operators and with permissions-descriptor'
dbg=$t capa=$t layouts=$t ops=$f toknat=$t mutran=$f single=$f nft=$f perdesc=$t lzep=$f lzepm=$f rule 'The perdesc_noops_contract but in debug mode'
dbg=$f capa=$t layouts=$t ops=$t toknat=$t mutran=$f single=$t nft=$f perdesc=$f lzep=$f lzepm=$f rule 'The default for single-asset'
dbg=$f capa=$t layouts=$t ops=$t toknat=$t mutran=$t single=$t nft=$f perdesc=$f lzep=$f lzepm=$f rule 'The single-asset with mutez transfer entry-point'
dbg=$f capa=$t layouts=$t ops=$t toknat=$t mutran=$t single=$f nft=$t perdesc=$f lzep=$f lzepm=$f rule 'The default in NFT mode with mutez transfer entry-point'
dbg=$f capa=$t layouts=$t ops=$t toknat=$t mutran=$f single=$f nft=$f perdesc=$f lzep=$t lzepm=$f rule 'The default with lazy-entry-points flag'
dbg=$f capa=$t layouts=$t ops=$t toknat=$t mutran=$f single=$f nft=$f perdesc=$f lzep=$f lzepm=$t rule 'The default with lazy-entry-points-multiple flag'
dbg=$f capa=$t layouts=$t ops=$t toknat=$t mutran=$t single=$f nft=$f perdesc=$f lzep=$t lzepm=$f rule 'The default with mutez-transfer and lazy-entry-points flag'
dbg=$f capa=$t layouts=$t ops=$t toknat=$t mutran=$t single=$f nft=$f perdesc=$f lzep=$f lzepm=$t rule 'The default with mutez-transfer and lazy-entry-points-multiple flag'



cat <<EOF

(rule
  (target fa2_contracts.ml)
  (deps $(for n in $(cat $names) ; do printf " fa2_interface_$n.ml fa2_contract_$n.ml" ; done ))
  (action
    (with-stdout-to %{target}
     (echo "
let version_string = \"${version_string}\"
let all = [
EOF
for n in $(cat $names) ; do
    cat <<EOF
  (module Fa2_contract_$n : Sigs.CONTRACT),
  (module Fa2_interface_$n : Sigs.CONTRACT_INTERFACE);
EOF
done
cat <<EOF
]
"))))

;; high-level aliases
(alias (name contracts)
  (deps $(for n in $(cat $names) ; do printf " fa2_$n.tz" ; done) ))
(alias (name simulations)
  (deps $(for n in $(cat $names) ; do printf " fa2_$n-simulation-log.txt" ; done) ))
EOF
