open! Base
open Common
open Io.Monad_infix

module Crypto = struct
  module Account = struct
    type t = {name: string; pkh: string; pk: string; sk: string}

    let make_csv state name =
      Fmt.kstr (Command.run state) "flextesa key-of-name %s" (Path.quote name)
      >>= fun flx ->
      let out = Command.Command_result.get_stdout state flx in
      Io.return out

    let of_seed state name =
      make_csv state name
      >>= fun out ->
      match out |> String.strip |> String.split ~on:',' with
      | [_; pk; pkh; sk] -> Io.return {name; pkh; pk; sk}
      | other ->
          Fmt.kstr Io.fail_system "Cannot parse key-of-name: %S %S" name
            (String.concat ~sep:"," other)

    let account_of_phrase_cmd () =
      let open Cmdliner in
      let open Term in
      ( ( const (fun state output seed ->
              Io.run (fun () ->
                  ( if
                    String.for_all seed ~f:(function
                      | 'a' .. 'z'
                       |'A' .. 'Z'
                       |'0' .. '9'
                       |'-' | '_' | '@' | ':' ->
                          true
                      | _ -> false)
                  then Io.return ()
                  else Io.fail_system "Seed phrase has forbidden characters" )
                  >>= fun () ->
                  make_csv state seed
                  >>= fun out ->
                  ( match output with
                  | None -> Fmt.pr "%s%!" out
                  | Some f -> Stdio.Out_channel.write_all f ~data:out ) ;
                  Io.return ()))
        $ State.cmdliner_term ()
        $ Arg.(
            value
              (opt (some string) None
                 (info ["output"; "o"] ~doc:"Output to a file.")))
        $ Arg.(
            required
              (pos 0 (some string) None
                 (info [] ~docv:"SEED PHRASE"
                    ~doc:"The seed to create the key from "))) )
      , info "account-of-seed" ~doc:"Generate an account CSV from a seed phrase"
      )
  end
end

module Client = struct
  module Connection = struct
    type t = Node of {address: string option; port: int; tls: bool}
    [@@deriving show, eq]

    let node ?address ?(tls = false) port = Node {address; port; tls}
    let default = node 20_000

    let cmdliner_term () =
      let open Cmdliner in
      let open Term in
      const (fun uris ->
          let uri = Uri.of_string uris in
          let address = Uri.host uri in
          let port =
            let default = 42_000 in
            match (Uri.port uri, Uri.path uri) with
            | None, "" -> default
            | Some p, "" -> p
            | Some p, more ->
                Console.warnf "not using %S from %a" more Uri.pp uri ;
                p
            | None, s -> (
              try Int.of_string s
              with _ -> (
                try Int.of_string (String.chop_prefix_exn s ~prefix:":")
                with _ ->
                  Console.warnf "cannot understand %S from %a, using port %d" s
                    Uri.pp uri default ;
                  default ) ) in
          match Uri.scheme uri with
          | None | Some "http" -> node ?address port
          | Some "https" -> node ~tls:true ?address port
          | Some other -> Fmt.failwith "Unknown URI scheme: %S" other)
      $ Arg.(
          value
            (opt string ":42000"
               (info ["connection"]
                  ~doc:"The URI to use to connect to the node.")))
  end

  type t =
    { client_command: string [@default "tezos-client"]
    ; connection: Connection.t [@main]
    ; mutable funder: string
    ; confirmation: [`Bake | `Wait of int] }
  [@@deriving make, show, eq]

  let of_uri uri =
    let tls = match Uri.scheme uri with Some "https" -> true | _ -> false in
    let address = Uri.host uri in
    let port = Uri.port uri |> Option.value ~default:20_002 in
    let connection = Connection.node ~tls ?address port in
    let funder = Path.basename (Uri.path uri) in
    let confirmation =
      match
        ( Uri.get_query_param uri "wait" |> Option.map ~f:Int.of_string
        , Uri.get_query_param uri "bake" |> Option.map ~f:Bool.of_string )
      with
      | Some n, None | Some n, Some false -> `Wait n
      | None, Some true -> `Bake
      | Some _, Some true ->
          Fmt.failwith "Cannot put wait bake=true in URI: %a" Uri.pp uri
      | None, Some false | None, None -> `Wait 1
      | exception _ -> Fmt.failwith "Wrong confirmation in URI: %a" Uri.pp uri
    in
    let client_command = Uri.get_query_param uri "command" in
    make connection ~funder ~confirmation ?client_command

  let default_uri =
    "http://:2020/unencrypted:edsk3S7mCwuuMVS21jsYTczxBU4tgTbQp98J3YmTGcstuUxsrZxKYd?bake=true"

  let uri_cmdliner_term () =
    let open Cmdliner in
    let open Term in
    const (fun uri ->
        let c = of_uri (Uri.of_string uri) in
        dbgf "Client: %a" pp c ; c)
    $ Arg.(
        let env =
          env_var ~doc:"Overrides the default for the --client option"
            "fatoo_client" in
        value
          (opt string default_uri
             (info ["client"] ~docv:"URI" ~doc:"Specify the client to use" ~env)))

  let show_uri_doc_cmd () =
    let document =
      let open Markdown in
      make ~header:[]
        [ par
            "The URI follows the usual pattern: \
             `<scheme>://<host>:<port>/<path>?<options>`:"
        ; itemize
            [ "`<scheme>` can be `http` or `http` (`--tls` option);"
            ; "`<host>:<port>` defines the connection to the node;"
            ; "`<path>` is the private-key (URI) for the “funder” account \
               which is used to pay for gas and storage." ]
        ; par "Available `<options>` are:"
        ; itemize
            [ "`bake=true`: use the `funder` account to also bake blocks after \
               injecting operations (useful for “manual” sandboxes);"
            ; "`wait=<INT>`: set the `--wait` option of `tezos-client` (how \
               many blocks to wait after an operation is injected);"
            ; "`command=<STRING>`: use an alternative command for \
               `tezos-client`." ]
        ; Fmt.kstr par "See for instance the current default: `%s`." default_uri
        ] in
    let msg = Fmt.const Markdown.print document in
    let open Cmdliner in
    let open Term in
    ( const (fun () -> Io.run (fun () -> Fmt.pr "%a\n%!" msg () ; Io.return ()))
      $ const ()
    , info "show-client-uri-documentation"
        ~doc:"Show more information on the client configuration URI." )

  let command ?(succeed = true) state client args =
    let base_dir = State.client_base_dir state in
    let command_string =
      let exec =
        let connect_args =
          match client.connection with
          | Node {address; port; tls} ->
              (if tls then ["--tls"] else [])
              @ ["--port"; Int.to_string port]
              @ Option.value_map address ~default:[] ~f:(fun a -> ["--addr"; a])
        in
        ["-d"; base_dir] @ connect_args @ args in
      String.concat ~sep:" "
        (client.client_command :: List.map exec ~f:Path.quote) in
    Command.run ~succeed state command_string

  let make_account ?sk state client name =
    ( match sk with
    | Some s -> Io.return s
    | None ->
        Crypto.Account.of_seed state name
        >>= fun account -> Io.return account.Crypto.Account.sk )
    >>= fun secret_key ->
    command state client ["import"; "secret"; "key"; name; secret_key; "--force"]
    >>= fun _ -> Io.return name

  let potential_bake state client =
    match client.confirmation with
    | `Wait _ -> dbgf "not baking here" ; Io.return ()
    | `Bake ->
        command state client
          ["bake"; "for"; client.funder; "--force"; "--minimal-timestamp"]
        >>= fun _ -> Io.return ()

  let init state client =
    let base_dir = State.client_base_dir state in
    Fmt.kstr (Command.run state) "mkdir -p %s" (Path.quote base_dir)
    >>= fun _ ->
    ( if String.is_prefix client.funder ~prefix:"unencrypted:" then (
      dbgf "We need to import %s" client.funder ;
      let name = "the-funding-authority" in
      make_account ~sk:client.funder state client name
      >>= fun _ ->
      client.funder <- name ;
      Io.return () )
    else Io.return () )
    >>= fun () ->
    List.init 3 ~f:Fn.id
    |> List.fold ~init:(Io.return ()) ~f:(fun pm _ ->
           pm >>= fun () -> potential_bake state client)

  let wait_arg state =
    match state.confirmation with `Wait n -> Int.to_string n | `Bake -> "none"

  let get_balance state client ~address =
    command state client
      [ "rpc"; "get"
      ; Fmt.str "/chains/main/blocks/head/context/contracts/%s/balance" address
      ]
    >>= fun cmd ->
    Io.catch (fun () ->
        match
          Command.Command_result.get_stdout state cmd
          |> Ezjsonm.value_from_string
        with
        | `String s -> Int.of_string s
        | _ -> failwith "Wrong JSON for balance RPC")

  let mutez_to_tez_string amount =
    let tez = amount / 1_000_000 in
    let decimals = amount % 1_000_000 in
    Fmt.str "%d%s" tez (if decimals = 0 then "" else Fmt.str ".%06d" decimals)

  let raw_transfer ?(extra_args = []) state client ~amount ~src ~dst =
    command state client
      ( [ "-wait"; wait_arg client; "transfer"; mutez_to_tez_string amount
        ; "from"; src; "to"; dst; "--burn-cap"; "100" ]
      @ extra_args )
    >>= fun rt ->
    let op_hash, branch, gas =
      let lines =
        Command.Command_result.get_stdout state rt
        |> String.split ~on:'\n'
        |> List.map ~f:(fun l ->
               String.split (String.strip l) ~on:' '
               |> List.filter_map ~f:(fun s ->
                      match String.strip s with "" -> None | t -> Some t)) in
      let op = ref None in
      let br = ref None in
      let gas = ref None in
      List.iter lines ~f:(function
        | "tezos-client"
          :: "wait"
             :: "for"
                :: o
                   :: "to"
                      :: "be"
                         :: "included"
                            :: "--confirmations" :: _ :: "--branch" :: b :: _ ->
            op := Some o ;
            br := Some b ;
            ()
        | "Consumed" :: "gas:" :: n :: _ ->
            gas := Option.try_with (fun () -> Int.of_string n)
        | _ -> ()) ;
      (!op, !br, !gas) in
    dbgf "raw-trasnfer: %a %a %a %a" Command.Command_result.pp rt
      Fmt.Dump.(option string)
      op_hash
      Fmt.Dump.(option string)
      branch
      Fmt.(Dump.option int)
      gas ;
    potential_bake state client >>= fun () -> Io.return (op_hash, branch, gas)

  module Transfer_report = struct
    type t = {balance_update: int * int; gas: int option} [@@deriving show, eq]

    let pp ppf t =
      let open Fmt in
      let b, a = t.balance_update in
      pf ppf "(%d→%d)" b a

    let make ?gas ~balance_update () : t = {balance_update; gas}
  end

  let rec ensure_account ?(funding_extra_percent = 145) state client
      (acc : Crypto.Account.t) ~balance =
    let account_name = acc.Crypto.Account.name in
    make_account state client ~sk:acc.sk account_name
    >>= fun name ->
    command state client ["get"; "balance"; "for"; name]
    >>= fun balance_cmd ->
    ( (* TODO: replace with RPC *)
    match
      Option.(
        String.lsplit2
          (Command.Command_result.get_stdout state balance_cmd)
          ~on:' '
        >>= fun (o, _) ->
        try_with (fun () -> Float.(of_string o * 1_000_000. |> to_int)))
    with
    | Some o -> Io.return o
    | None -> Fmt.kstr Io.fail_system "Failed to get the balance for %s" acc.pkh
    )
    >>= fun current_balance ->
    if balance <= current_balance then Io.return account_name
    else
      raw_transfer state client ~dst:name
        ~amount:((balance - current_balance) * funding_extra_percent / 100)
        ~src:client.funder
      >>= fun (op, _, _) ->
      dbgf "Made %s with %d (cur: %d, op: %a)" name balance current_balance
        Fmt.Dump.(option string)
        op ;
      Io.return name

  and transfer ?(gas_money = 10_000_000) ?arg ?entry_point ?(amount = 0) state
      client ~src ~dst =
    let extra_args =
      Option.value_map arg ~default:[] ~f:(fun a -> ["--arg"; a])
      @ Option.value_map entry_point ~default:[] ~f:(fun e ->
            ["--entrypoint"; e]) in
    ensure_account state client src ~balance:(gas_money + (20 * amount))
    >>= fun src_name ->
    get_balance state client ~address:src.Crypto.Account.pkh
    >>= fun balance_before ->
    raw_transfer state client ~amount ~extra_args ~src:src_name ~dst
    >>= fun (_op, _br, gas) ->
    get_balance state client ~address:src.Crypto.Account.pkh
    >>= fun balance_after ->
    Io.return
      (Transfer_report.make ?gas
         ~balance_update:(balance_before, balance_after)
         ())

  let originate ?(transferring = "0") ?(arg = "Unit") ?from state client ~name
      contract =
    let tz_file =
      match contract with
      | `File f -> f
      | `Literal code ->
          let tz_file = Path.temp_file "test" "contract.tz" in
          Stdio.Out_channel.write_all tz_file ~data:code ;
          tz_file in
    let from = Option.value from ~default:client.funder in
    command state client
      [ "--block"; "head"; "--wait"; wait_arg client; "originate"; "contract"
      ; name; "transferring"; transferring; "from"; from; "running"; tz_file
      ; "--force"; "--init"; arg; "--burn-cap"; "20" ]
    >>= fun _origination ->
    potential_bake state client
    >>= fun () ->
    command state client ["show"; "known"; "contract"; name]
    >>= fun contract ->
    let address =
      Command.Command_result.get_stdout state contract |> String.strip in
    Io.return address

  let get_rpc_json state client ~path =
    command state client ["rpc"; "get"; path]
    >>= fun storage ->
    Io.catch (fun () ->
        Command.Command_result.get_stdout state storage
        |> Ezjsonm.value_from_string)

  let get_storage_json state client ~address =
    get_rpc_json state client
      ~path:
        (Fmt.str "/chains/main/blocks/head/context/contracts/%s/storage"
           address)
end

let test state (client : Client.t) () =
  let open Fa2_interface_dbg_contract in
  let module Fa2 = Fa2_contract_dbg_contract in
  dbgf "Using client: %a" Client.pp client ;
  let tz_file = Path.temp_file "test" "contract.tz" in
  Stdio.Out_channel.write_all tz_file ~data:Fa2.code ;
  dbgf "Wrote: %s" tz_file ;
  Client.init state client
  >>= fun () ->
  Client.command state client ["bootstrapped"]
  >>= fun bootstrapped ->
  dbgf "Bootstrapped: %s" (Command.Command_result.get_stdout state bootstrapped) ;
  Crypto.Account.of_seed state "Admin-0"
  >>= fun admin ->
  let arg = Fa2.init ~admin:admin.Crypto.Account.pkh in
  Client.originate state client ~arg ~name:Fa2.name (`File tz_file)
  >>= fun address ->
  dbgf "Contract: %S" address ;
  Client.get_storage_json state client ~address
  >>= fun json ->
  Storage.of_json json
  >>= fun storage_parsed ->
  dbgf "Storage: %a" Storage.pp storage_parsed ;
  Crypto.Account.of_seed state "alice"
  >>= fun alice ->
  Crypto.Account.of_seed state "bob"
  >>= fun bob ->
  let mint_0 =
    let open Parameter in
    Mint
      (Mint.make
         ~amount:M_nat.(Int 42)
         ~address:M_address.(Raw_b58 alice.Crypto.Account.pkh)
         ~symbol:M_string.(Raw_string "SM0")
         ~token_id:M_nat.(Int 0)) in
  let `Name mint_ep, `Literal mint_arg =
    Parameter.to_concrete_entry_point mint_0 in
  Client.transfer state client ~dst:address ~entry_point:mint_ep ~arg:mint_arg
    ~src:admin
  >>= fun _ ->
  Client.get_storage_json state client ~address
  >>= fun json ->
  Storage.of_json json
  >>= fun storage_parsed ->
  dbgf "Storage: %a" Storage.pp storage_parsed ;
  let transfer =
    let open Parameter in
    let one f t a =
      Transfer_element.make
        ~from_:M_address.(Raw_b58 f.Crypto.Account.pkh)
        ~txs:
          [ Txs_element.make
              ~amount:M_nat.(Int a)
              ~to_:M_address.(Raw_b58 t.Crypto.Account.pkh)
              ~token_id:M_nat.(Int 0) ] in
    Transfer [one alice bob 1; one alice admin 1; one alice admin 1] in
  let `Name transfer_ep, `Literal transfer_arg =
    Parameter.to_concrete_entry_point transfer in
  Client.transfer state client ~dst:address ~entry_point:transfer_ep
    ~arg:transfer_arg ~src:admin
  >>= fun _ ->
  Client.get_storage_json state client ~address
  >>= fun json ->
  Storage.of_json json
  >>= fun storage_parsed ->
  dbgf "Storage: %a" Storage.pp storage_parsed ;
  dbgf "%a"
    Fmt.(
      vbox
        ( box ~indent:2
            (const string "Transfer:" ++ sp ++ const Parameter.pp transfer)
        ++ cut
        ++ box ~indent:2
             ( const string "Concrete: " ++ sp
             ++ const string (Parameter.to_concrete transfer) )
        ++ box ~indent:2
             ( const string "Entry-point: "
             ++ sp
             ++
             let `Name n, `Literal l =
               Parameter.to_concrete_entry_point transfer in
             fun ppf () -> pf ppf "%S -> %s" n l ) ))
    () ;
  List.iter Fa2_contracts.all ~f:(fun (m, _) ->
      let module Meta = (val m) in
      dbgf "@[<2>Contract %S:@ %a@]" Meta.name Sigs.Switches.pp Meta.switches) ;
  Io.return ()

module Command_line_helpers = struct
  let contract_arg =
    let open Cmdliner in
    Arg.enum
      (List.map Fa2_contracts.all ~f:(fun (m, i) ->
           let module Meta = (val m) in
           (Meta.name, (m, i))))

  let default_contract = List.hd_exn Fa2_contracts.all

  let all_or_a_few_contracts_pos_all_term () =
    let open Cmdliner in
    let open Term in
    ret
      ( const (fun all l ->
            match (all, l) with
            | false, [] -> `Error (false, "No contracts provided.")
            | true, [] -> `Ok Fa2_contracts.all
            | false, l -> `Ok l
            | true, _ ->
                `Error
                  ( false
                  , "Cannot provide option `--all-known-contracts` and give \
                     contract name at the same time." ))
      $ Arg.(
          value
            (flag
               (info ["all-known-contracts"]
                  ~doc:"Make this happen for all known contracts.")))
      $ Arg.(
          value
            (pos_all contract_arg []
               (info [] ~docv:"CONTRACT-NAMES" ~doc:"Contracts to originate.")))
      )
end

module Tez = struct let f fl = Float.(fl * 1_000_000. |> to_int) end

module Benchmark = struct
  module Experiment = struct
    module Batching_style = struct
      type t = [`Factor_from | `Repeat_from] [@@deriving show]

      let to_string = function
        | `Factor_from -> "factor-from"
        | `Repeat_from -> "repeat-from"

      let cmdliner_converter : t list Cmdliner.Arg.conv =
        let open Cmdliner in
        let open Arg in
        let s x = (to_string x, [x]) in
        enum
          [ s `Factor_from; s `Repeat_from
          ; ("both", [`Repeat_from; `Factor_from]) ]
    end

    type t =
      | Basic of
          { contract: (module Sigs.CONTRACT) * (module Sigs.CONTRACT_INTERFACE)
          ; operators: int
          ; batch_size: int
          ; batching_style: Batching_style.t }

    let basic ~contract ~operators ~batch_size ~batching_style =
      Basic {contract; operators; batch_size; batching_style}

    let pp ppf
        (Basic {contract= m, _; operators; batch_size; batching_style} as exp) =
      let module Contract = (val m : Sigs.CONTRACT) in
      let open Fmt in
      let f =
        hovbox ~indent:2
          ( const string "Basic-experiment:"
          ++ sp
          ++ const
               (list ~sep:sp (pair (string ++ const string ":") string))
               [ ("Contract", Contract.name)
               ; ("Operators", Int.to_string operators)
               ; ("Batch-size", Int.to_string batch_size)
               ; ("batching-style", Batching_style.to_string batching_style) ]
          ) in
      f ppf exp
  end

  let run state client ~dry_run ~contracts ~total_transfers ~batch_sizes
      ~token_ids ~operators ~batching_styles () =
    (* First bench:
       mint n to alice;
       repeat n/5 time or until fail
       transfer 5 from alice to new_address
       record transfer-nb, price
       then same after adding k operators for alice
    *)
    let experiments =
      List.concat_map batching_styles ~f:(fun batching_style ->
          List.concat_map contracts ~f:(fun c ->
              List.concat_map batch_sizes ~f:(fun batch_size ->
                  let module Contract = (val fst c : Sigs.CONTRACT) in
                  List.concat_map operators ~f:(fun operators ->
                      if
                        operators = 0
                        || Contract.switches.Sigs.Switches.support_operator
                      then
                        [ Experiment.basic ~contract:c ~operators
                            ~batching_style ~batch_size ]
                      else [])))) in
    Console.say Fmt.(const (list ~sep:cut Experiment.pp) experiments) ;
    if dry_run then (
      Console.warn Fmt.(const text "Dry-run done.") ;
      Io.return () )
    else
      Crypto.Account.of_seed state "admin-0"
      >>= fun administrator ->
      Client.init state client
      >>= fun () ->
      Client.command state client ["bootstrapped"]
      >>= fun bootstrapped ->
      dbgf "Bootstrapped: %s"
        (Command.Command_result.get_stdout state bootstrapped) ;
      let tsv_path = state.State.root_path // Fmt.str "results.tsv" in
      let tsv_out_channel = Stdio.Out_channel.create tsv_path in
      let tsv_types = ref [] in
      let out_tsv l =
        let typ, values =
          List.map l ~f:(function
            | t, `S s when String.equal t s -> ((t, `Kind), s)
            | t, `S s -> ((t, `String), s)
            | t, `I i -> ((t, `Int), Int.to_string i))
          |> List.unzip in
        let string_of_typ (s, k) =
          Fmt.str "%s:%s" s
            ( match k with
            | `Kind -> "kind"
            | `String -> "string"
            | `Int -> "int" ) in
        ( match typ with
        | [] -> assert false
        | kind :: _
          when List.exists !tsv_types ~f:(fun l ->
                   String.(List.hd_exn l = string_of_typ kind)) ->
            ()
        | _ -> tsv_types := List.map ~f:string_of_typ typ :: !tsv_types ) ;
        Stdio.Out_channel.fprintf tsv_out_channel "%s\n%!"
          (values |> String.concat ~sep:"\t") in
      let results =
        List.map experiments ~f:(function
            | Experiment.Basic
                {contract= m, i; operators; batch_size; batching_style}
            ->
            let module Contract = (val m : Sigs.CONTRACT) in
            let module Interface = (val i : Sigs.CONTRACT_INTERFACE) in
            let code = Contract.code in
            let admin_pkh = administrator.Crypto.Account.pkh in
            let arg = Contract.init ~admin:admin_pkh in
            let name = Contract.name in
            dbgf "Originating %s(%s)" name administrator.Crypto.Account.pkh ;
            Client.ensure_account state client administrator
              ~balance:(Tez.f 30.)
            >>= fun admin_name ->
            Client.get_balance state client ~address:admin_pkh
            >>= fun before_origination ->
            Client.originate state client ~arg ~name (`Literal code)
              ~from:admin_name
            >>= fun address ->
            Client.get_balance state client ~address:admin_pkh
            >>= fun after_origination ->
            dbgf "Originated %s -> %s %d → %d" name address before_origination
              after_origination ;
            out_tsv
              [ ("origination", `S "origination"); ("flavor", `S Contract.name)
              ; ("address", `S address)
              ; ("balance-before", `I before_origination)
              ; ("balance-after", `I after_origination) ] ;
            Crypto.Account.of_seed state "alice"
            >>= fun alice ->
            Client.ensure_account state client alice ~balance:(Tez.f 3_000.)
            >>= fun _alice_name ->
            let total_tokens = total_transfers * batch_size in
            List.fold ~init:(Io.return ()) token_ids ~f:(fun pm i ->
                pm
                >>= fun () ->
                let mint =
                  let open Interface in
                  Mint.make
                    ~amount:M_nat.(Int total_tokens)
                    ~address:M_address.(Raw_b58 alice.Crypto.Account.pkh)
                    ~symbol:M_string.(Raw_string "SM0")
                    ~token_id:M_nat.(Int i) in
                let mint_arg = Interface.Mint.to_concrete mint in
                Client.transfer state client ~dst:address ~entry_point:"mint"
                  ~arg:mint_arg ~src:administrator
                >>= fun mint_report ->
                out_tsv
                  [ ("mint", `S "mint"); ("flavor", `S Contract.name)
                  ; ("address", `S address); ("token-id", `I i)
                  ; ("balance-before", `I (fst mint_report.balance_update))
                  ; ("balance-after", `I (snd mint_report.balance_update))
                  ; ( "consumed-gas"
                    , `I (Option.value mint_report.gas ~default:(-1)) ) ] ;
                let ops = List.init operators ~f:(Fmt.str "op%d") in
                List.fold ~init:(Io.return []) ops ~f:(fun pm op ->
                    pm
                    >>= fun p ->
                    Crypto.Account.of_seed state op
                    >>= fun acc -> Io.return (acc :: p))
                >>| List.rev
                >>= fun op_accounts ->
                let rec add_ops operators =
                  match operators with
                  | [] -> Io.return ()
                  | more ->
                      let now, later = List.split_n more 10 in
                      let add_op =
                        let open Interface in
                        List.map now ~f:(fun acc ->
                            Update_operators_element.Add_operator
                              Add_operator.(
                                make
                                  ~operator:
                                    M_address.(Raw_b58 acc.Crypto.Account.pkh)
                                  ~owner:
                                    M_address.(Raw_b58 alice.Crypto.Account.pkh)
                                  ~token_id:(M_nat.Int 0)
                                (* ~tokens:(Tokens.All_tokens M_unit.Unit) *)))
                      in
                      let update_arg =
                        Interface.(
                          M_list.to_concrete
                            Update_operators_element.to_concrete)
                          add_op in
                      dbgf "update_operators(add {%s}), (tok-%d)"
                        ( List.map now ~f:(fun acc -> acc.Crypto.Account.name)
                        |> String.concat ~sep:"," )
                        i ;
                      Client.transfer state client ~dst:address
                        ~entry_point:"update_operators" ~arg:update_arg
                        ~src:administrator
                      >>= fun ao_report ->
                      out_tsv
                        [ ("add-operators", `S "add-operators")
                        ; ("flavor", `S Contract.name); ("address", `S address)
                        ; ("operators", `I (List.length now))
                        ; ("balance-before", `I (fst ao_report.balance_update))
                        ; ("balance-after", `I (snd ao_report.balance_update))
                        ; ( "consumed-gas"
                          , `I (Option.value ao_report.gas ~default:(-1)) ) ] ;
                      add_ops later in
                add_ops op_accounts)
            >>= fun () ->
            Client.get_storage_json state client ~address
            >>= fun json ->
            Io.dispatch
              (Interface.Storage.of_json json)
              ~ok:(fun storage ->
                dbgf "Before loop: %a" Interface.Storage.pp storage ;
                Io.return ())
              ~error:(fun (`Of_json _) ->
                dbgf "Before loop: OF-JSON error: %s" address ;
                Io.return ())
            >>= fun () ->
            let rec loop acc count =
              if count > total_transfers then Io.return acc
              else
                let dst_names =
                  List.init batch_size ~f:(Fmt.str "bob%d-%d" count) in
                List.fold dst_names ~init:(Io.return []) ~f:(fun prevm name ->
                    prevm
                    >>= fun l ->
                    Crypto.Account.of_seed state name
                    >>= fun acc -> Io.return (acc :: l))
                >>| List.rev
                >>= fun accounts ->
                let batch =
                  let open Interface in
                  let tx t a =
                    let chosen_token = List.random_element_exn token_ids in
                    Txs_element.make
                      ~amount:M_nat.(Int a)
                      ~to_:M_address.(Raw_b58 t.Crypto.Account.pkh)
                      ~token_id:M_nat.(Int chosen_token) in
                  match batching_style with
                  | `Repeat_from ->
                      let one f t a =
                        Transfer_element.make
                          ~from_:M_address.(Raw_b58 f.Crypto.Account.pkh)
                          ~txs:[tx t a] in
                      List.map accounts ~f:(fun bob_n -> one alice bob_n 1)
                  | `Factor_from ->
                      [ (let txs =
                           List.map accounts ~f:(fun bob_n -> tx bob_n 1) in
                         Transfer_element.make
                           ~from_:M_address.(Raw_b58 alice.Crypto.Account.pkh)
                           ~txs) ] in
                let arg =
                  Interface.(M_list.to_concrete Transfer_element.to_concrete)
                    batch in
                dbgf "transfer %d %s" count arg ;
                let out_record kind tail =
                  out_tsv
                    ( [ (kind, `S kind); ("flavor", `S Contract.name)
                      ; ("address", `S address); ("ith", `I count)
                      ; ("operators-nb", `I operators)
                      ; ("tokens-nb", `I (List.length token_ids))
                      ; ("batch-size", `I batch_size)
                      ; ( "batching-style"
                        , `S
                            (Experiment.Batching_style.to_string batching_style)
                        ) ]
                    @ tail ) in
                Io.dispatch
                  (Client.transfer state client ~dst:address
                     ~gas_money:1_000_000_000 ~entry_point:"transfer" ~arg
                     ~src:alice)
                  ~ok:(fun report ->
                    out_record "transfer"
                      Client.Transfer_report.
                        [ ("balance-before", `I (fst report.balance_update))
                        ; ("balance-after", `I (snd report.balance_update))
                        ; ( "consumed-gas"
                          , `I (Option.value report.gas ~default:(-1)) ) ] ;
                    loop (Ok report :: acc) (count + 1))
                  ~error:(fun (`System (m, l)) ->
                    let error_summary =
                      let s =
                        Fmt.(
                          str "%s%a" m (list ~sep:sp (fun ppf f -> f ppf ())) l)
                      in
                      let open Option in
                      let f pattern name =
                        String.substr_index s ~pattern >>= fun _ -> return name
                      in
                      first_some
                        (f "Oversized operation" "oversized-operation")
                        (f "Gas limit exceeded" "gas-limit-exceeded")
                      |> value ~default:"unknown-error" in
                    out_record "failed-transfer" [("error", `S error_summary)] ;
                    dbgf "Error: %s@ %a" m
                      Fmt.(
                        fun ppf () ->
                          List.iter l ~f:(fun f -> cut ppf () ; f ppf ()))
                      () ;
                    Io.return (Error "error" :: acc)) in
            loop [] 1
            >>= fun _reports ->
            (* let contract = Fmt.str "%s-%do" name operators in
               let description =
                 Fmt.str "%s (%d operators) (batch: %d)" Contract.description
                   operators batch_size in *)
            Io.return ()) in
      List.fold results
        ~init:(Io.return ([], []))
        ~f:(fun prevm res ->
          prevm
          >>= fun (prev_runs, prev_errors) ->
          match res with
          | Ok report -> Io.return (report :: prev_runs, prev_errors)
          | Error (`Of_json _ as e) -> Io.fail e
          | Error (`System (e, _)) ->
              dbgf "error: %s" e ;
              Io.return (prev_runs, e :: prev_errors))
      >>= fun (_runs, errors) ->
      Stdio.Out_channel.close tsv_out_channel ;
      (* let report =
         Report.make ~token_ids ~total_transfers ~runs:(List.rev runs) () in *)
      (* let path = state.State.root_path // Fmt.str "results.bin" in *)
      let tsv_types_path =
        state.State.root_path // Fmt.str "results-types.tsv" in
      Stdio.Out_channel.(
        with_file tsv_types_path ~f:(fun o ->
            List.iter !tsv_types ~f:(fun l ->
                fprintf o "%s\n%!" (String.concat ~sep:"\t" l)))) ;
      (* dbgf "Saved at %s" path ;
         Report.save report ~path ; *)
      Console.say
        Fmt.(
          const text "Results saved at:"
          ++ sp
          ++ vbox (const string tsv_path ++ cut ++ const string tsv_types_path)) ;
      match errors with
      | [] -> Io.return ()
      | more -> Io.fail_system (String.concat ~sep:"," more)

  let cmd () =
    let open Cmdliner in
    let open Term in
    let range_conv =
      let open Arg in
      let printer = Fmt.(list ~sep:(const string ",") int) in
      let parser s =
        try
          let get_int s =
            try Int.of_string s
            with _ -> failwith "Wrong format for batch-size" in
          match String.split ~on:'-' s with
          | [one] -> Ok (String.split ~on:',' one |> List.map ~f:get_int)
          | [one; two] ->
              let l = get_int one in
              let r = get_int two in
              Ok (List.init (r - l + 1) ~f:(fun i -> i + l))
          | _ -> failwith "Wrong interval format for batch-size"
        with Failure s -> Error (`Msg s) in
      conv ~docv:"RANGE-OR-LIST" (parser, printer) in
    ( const
        (fun state
             client
             contracts
             dry_run
             total_transfers
             batch_sizes
             token_ids
             operators
             batching_styles
             ()
             ->
          Io.run
            (run state ~dry_run client ~contracts ~total_transfers ~batch_sizes
               ~token_ids ~operators ~batching_styles))
      $ State.cmdliner_term ()
      $ Client.uri_cmdliner_term ()
      $ Command_line_helpers.all_or_a_few_contracts_pos_all_term ()
      $ Arg.(value (flag (info ["dry-run"] ~doc:"Don't run just display.")))
      $ Arg.(
          value
            (opt int 10
               (info ["total-transfers"] ~doc:"Total number of transfers")))
      $ Arg.(
          value
            (opt range_conv [1]
               (info ["batch-sizes"] ~doc:"Number of transfers per call")))
      $ Arg.(
          value
            (opt (list int) [0]
               (info ["tokens"] ~doc:"Token-ids to pick from.")))
      $ Arg.(
          value
            (opt range_conv [0] (info ["operators"] ~doc:"Operators to add.")))
      $ Arg.(
          value
            (opt Experiment.Batching_style.cmdliner_converter [`Repeat_from]
               (info ["batching-styles"] ~doc:"Set how to prepare the batches.")))
      $ const ()
    , info "run-benchmark" ~doc:"Run a benchmark" )
end

let cmd_self_test () =
  let open Cmdliner in
  let open Term in
  ( const (fun state client () -> Io.run (test state client))
    $ State.cmdliner_term ()
    $ Client.uri_cmdliner_term ()
    $ const ()
  , info "self-test" ~doc:"Random test" )

let cmd_list_contract_variants () =
  let format_of_string s =
    let options s =
      let l =
        s |> String.split ~on:',' |> List.map ~f:String.strip
        |> List.sort ~compare:String.compare in
      let rec go = function
        | [] -> []
        | "with-header" :: more -> `With_header :: go more
        | other :: _ -> Fmt.failwith "Unknown format option: %S" other in
      go l in
    match
      String.lowercase s |> String.split ~on:':' |> List.map ~f:String.strip
    with
    | ["csv"] -> `Separated (`Comma, [])
    | ["tsv"] -> `Separated (`Tab, [])
    | ["csv"; opts] -> `Separated (`Comma, options opts)
    | ["tsv"; opts] -> `Separated (`Tab, options opts)
    | ["markdown"] | ["md"] -> `Markdown []
    | _ -> Fmt.failwith "Wrong format format: %S" s in
  let all_names =
    List.map Fa2_contracts.all ~f:(fun (m, _) ->
        let module Meta = (val m) in
        (Meta.name, (Meta.metadata_string, Meta.description, Meta.switches)))
  in
  let sep = function `Comma -> "," | `Tab -> "\t" in
  let details_names =
    [ "version_string"; "description"; "debug"; "carthage_pairs"; "force_layouts"
    ; "support_operator"; "assume_consecutive_token_ids"; "add_mutez_transfer"
    ; "single_asset"; "non_fungible"; "add_permissions_descriptor"
    ; "lazy_entry_points"; "lazy_entry_points_multiple" ] in
  let details_list ?(on_desc = Fn.id) ?(on_metadata = Fn.id) bool_to_string
      ( metadata_string
      , desc
      , { Sigs.Switches.debug
        ; carthage_pairs
        ; force_layouts
        ; support_operator
        ; assume_consecutive_token_ids
        ; add_mutez_transfer
        ; single_asset
        ; non_fungible
        ; add_permissions_descriptor
        ; lazy_entry_points
        ; lazy_entry_points_multiple } ) =
    on_metadata metadata_string
    :: on_desc desc
    :: List.map ~f:bool_to_string
         [ debug; carthage_pairs; force_layouts; support_operator
         ; assume_consecutive_token_ids; add_mutez_transfer; single_asset
         ; non_fungible; add_permissions_descriptor; lazy_entry_points
         ; lazy_entry_points_multiple ] in
  let details separator dets =
    let details =
      details_list Bool.to_string dets
      |> List.map ~f:(fun s ->
             if Poly.(separator = `Comma) && String.contains s ',' then
               Fmt.str "%S" s
             else s) in
    String.concat ~sep:(sep separator) details
    (* ( metadata_string
       :: (match separator with `Comma -> Fmt.str "%S" desc | `Tab -> desc)
       :: List.map ~f:Bool.to_string
            [ debug; carthage_pairs; force_layouts; support_operator
            ; assume_consecutive_token_ids; add_mutez_transfer; single_asset ] ) *)
  in
  let run ~with_details ~with_header ~separator () =
    let sep = sep separator in
    let details_header = details_names |> String.concat ~sep in
    if with_header then
      Fmt.pr "%s%s\n%!" "NAME"
        ( match with_details with
        | `All -> sep ^ details_header
        | `None -> ""
        | `Description -> sep ^ "description" ) ;
    List.iter all_names ~f:(fun (n, sw) ->
        Fmt.pr "%s%s\n%!" n
          ( match with_details with
          | `All -> sep ^ details separator sw
          | `None -> ""
          | `Description -> sep ^ (fun (_, d, _) -> d) sw )) ;
    Io.return () in
  let show_markdown ~with_details () =
    let print ppf () =
      let bool_to_string = function true -> "✅" | false -> "⛌" in
      let on_metadata = Fmt.str "`%s`" in
      let on_desc = Fmt.str "%s." in
      let open Fmt in
      List.iter all_names ~f:(fun (n, ((_, d, _) as dets)) ->
          pf ppf "@,* `%s`" n ;
          match with_details with
          | `None -> pf ppf "."
          | `All ->
              pf ppf ":@,%a"
                (vbox
                   (list ~sep:(any "@,")
                      (pair ~sep:(any ": ")
                         (any "    * `" ++ string ++ any "`")
                         string)))
                (List.zip_exn details_names
                   (details_list ~on_metadata ~on_desc bool_to_string dets))
          | `Description -> pf ppf ": %s." d) ;
      pf ppf "@," in
    Fmt.pr "%a%!" (Fmt.vbox print) () ;
    Io.return () in
  let open Cmdliner in
  let open Term in
  ( const (fun with_details output_format () ->
        Io.run (fun () ->
            match output_format with
            | `Separated (separator, opts) ->
                let with_header = List.mem opts `With_header ~equal:Poly.equal in
                run ~with_details ~with_header ~separator ()
            | `Markdown _opts -> show_markdown ~with_details ()))
    $ Arg.(
        value
          (opt
             (enum
                [("all", `All); ("description", `Description); ("none", `None)])
             `None
             (info ["details"] ~doc:"Output details.")))
    $ Arg.(
        value
          (opt
             (conv
                ( (fun s ->
                    try Ok (format_of_string s)
                    with Failure s -> Error (`Msg s))
                , Fmt.any "TODO" ))
             (`Separated (`Tab, [`With_header]))
             (info ["format"] ~doc:"Set the output format.")))
    $ const ()
  , info "list-contract-variants"
      ~doc:"List the names of the available token variants." )

let cmd_print_code () =
  let run (m, _) ~output () =
    let module Meta = (val m : Sigs.CONTRACT) in
    let output_f data =
      match output with
      | None -> Stdio.Out_channel.(output_string stdout) data
      | Some f -> Stdio.Out_channel.(write_all f) ~data in
    output_f Meta.code ; Io.return () in
  let open Cmdliner in
  let open Term in
  ( const (fun contract output () -> Io.run (run contract ~output))
    $ Arg.(
        required
          (pos 0
             (some Command_line_helpers.contract_arg)
             None
             (info [] ~doc:"Contract" ~docv:"CONTRACT-NAME")))
    $ Arg.(
        value
          (opt (some string) None
             (info ["output"; "o"] ~doc:"Output to a file")))
    $ const ()
  , info "get-code" ~doc:"Get the code for a contract" )

module Make_expressions = struct
  let of_spec ~contract:(m, i) spec =
    let module Meta = (val m : Sigs.CONTRACT) in
    let module Itf = (val i : Sigs.CONTRACT_INTERFACE) in
    let open Itf in
    match spec with
    | `Init_storage address -> Meta.init ~admin:address
    | `Batch_transfer [] -> "{}"
    | `Batch_transfer (`Transfer_item (f, t, a, k) :: more) ->
        let from =
          Option.value_exn f
            ~message:
              "Error: at least the first transfer should have a `from:` field"
        in
        let tx t a k =
          Txs_element.make
            ~to_:M_address.(Raw_b58 t)
            ~amount:M_nat.(Int a)
            ~token_id:M_nat.(Int k) in
        let tr f txs = Transfer_element.make ~from_:M_address.(Raw_b58 f) ~txs in
        let last_from, rev_txs, rev_batch =
          List.fold more
            ~init:(from, [tx t a k], [])
            ~f:
              (fun (cur_from, rev_txs, rev_batch) (`Transfer_item (f, t, a, k)) ->
              match f with
              | None -> (cur_from, tx t a k :: rev_txs, rev_batch)
              | Some f when String.equal cur_from f ->
                  (cur_from, tx t a k :: rev_txs, rev_batch)
              | Some new_one ->
                  ( new_one
                  , [tx t a k]
                  , tr cur_from (List.rev rev_txs) :: rev_batch )) in
        List.rev (tr last_from (List.rev rev_txs) :: rev_batch)
        |> M_list.to_concrete Transfer_element.to_concrete
    | `Mint (addr, amount, id, symbol) ->
        Mint.make
          ~address:M_address.(Raw_b58 addr)
          ~amount:M_nat.(Int amount)
          ~token_id:M_nat.(Int id)
          ~symbol:M_string.(Raw_string symbol)
        |> Mint.to_concrete
    | `Update_operators l ->
        let open Update_operators_element in
        List.map l ~f:(function
          | `Add, (own, op, tok) (* , toks *) ->
              Add_operator
                (Add_operator.make
                   ~operator:M_address.(Raw_b58 op)
                   ~owner:M_address.(Raw_b58 own)
                   ~token_id:
                     (Int tok)
                     (* ~tokens:
                        Tokens.(
                          match toks with
                          | [] -> All_tokens M_unit.Unit
                          | more ->
                              Some_tokens (List.map more ~f:(fun i -> M_nat.Int i))) *))
          | `Remove, (own, op, tok) (* , toks *) ->
              Remove_operator
                (Remove_operator.make
                   ~operator:M_address.(Raw_b58 op)
                   ~owner:M_address.(Raw_b58 own)
                   ~token_id:
                     (Int tok)
                     (* ~tokens:
                        Tokens.(
                          match toks with
                          | [] -> All_tokens M_unit.Unit
                          | more ->
                              Some_tokens (List.map more ~f:(fun i -> M_nat.Int i))) *)))
        |> M_list.to_concrete to_concrete

  let cmd_print_expression ?(end_doc_sentence = "entry-point") ~entry_point
      ~argument () =
    let run contract ~output ~entrypoint () =
      let output_f data =
        match output with
        | None -> Stdio.Out_channel.(output_string stdout) data
        | Some f -> Stdio.Out_channel.(write_all f) ~data in
      let data = of_spec ~contract entrypoint in
      output_f (data ^ "\n") ;
      Io.return () in
    let open Cmdliner in
    let open Term in
    ( const (fun contract output entrypoint () ->
          Io.run (run contract ~entrypoint ~output))
      $ Arg.(
          value
            (opt Command_line_helpers.contract_arg
               Command_line_helpers.default_contract
               (info ["contract-flavor"] ~doc:"Contract" ~docv:"CONTRACT-NAME")))
      $ Arg.(
          value
            (opt (some string) None
               (info ["output"; "o"] ~doc:"Output to a file")))
      $ argument $ const ()
    , info
        (Fmt.str "make-%s-expression" entry_point)
        ~doc:
          (Fmt.str "Build a Michelson expression for the %s %s." entry_point
             end_doc_sentence) )

  let cmd_call_entry_point ~entry_point ~argument () =
    let run state client ~src ~dst ~contract ~output ~entrypoint () =
      let output_f data =
        match output with
        | None -> Stdio.Out_channel.(output_string stdout) data
        | Some f -> Stdio.Out_channel.(write_all f) ~data in
      let data = of_spec ~contract entrypoint in
      Client.init state client
      >>= fun () ->
      Client.make_account state client ~sk:src "minter"
      >>= fun src ->
      Client.raw_transfer state client ~amount:0 ~dst ~src
        ~extra_args:["--entrypoint"; entry_point; "--arg"; data]
      >>= fun _ ->
      output_f (data ^ "\n") ;
      Io.return () in
    let open Cmdliner in
    let open Term in
    ( const (fun state client dst src contract output entrypoint () ->
          Io.run (run state client ~dst ~src ~contract ~entrypoint ~output))
      $ State.cmdliner_term ()
      $ Client.uri_cmdliner_term ()
      $ Arg.(
          required
            (opt (some string) None
               (info ["address"] ~doc:"Address of the contract.")))
      $ Arg.(
          required
            (opt (some string) None
               (info ["source"] ~doc:"The source account.")))
      $ Arg.(
          value
            (opt Command_line_helpers.contract_arg
               Command_line_helpers.default_contract
               (info ["contract-flavor"] ~doc:"Contract" ~docv:"CONTRACT-NAME")))
      $ Arg.(
          value
            (opt (some string) None
               (info ["output"; "o"] ~doc:"Output to a file")))
      $ argument $ const ()
    , info
        (Fmt.str "call-%s"
           (String.map ~f:(function '_' -> '-' | c -> c) entry_point))
        ~doc:(Fmt.str "Call the %s entry-point." entry_point) )

  let print_and_call_cmds ~entry_point ~argument () =
    [ cmd_print_expression () ~entry_point ~argument
    ; cmd_call_entry_point () ~entry_point ~argument ]

  module Parse_records = struct
    type nat = Type_nat
    type address = Type_address

    type _ t =
      | Nil : unit t
      | Nat : {field: string; right: 'b t} -> (int * 'b) t
      | Address : {field: string; right: 'b t} -> (string * 'b) t
      | Address_option : {field: string; right: 'b t} -> (string option * 'b) t

    type 'a record = 'a t

    let nat_field f right = Nat {field= f; right}
    let address_field f right = Address {field= f; right}
    let address_option_field f right = Address_option {field= f; right}

    let parse_record r s ~f =
      let open Angstrom in
      let natural =
        take_while1 (function '0' .. '9' | '_' -> true | _ -> false)
        >>| Int.of_string
        >>= fun i -> return (`Nat i) <?> "natural-number" in
      let address =
        take_while1 (function
          | '0' .. '9' | 'a' .. 'z' | 'A' .. 'Z' -> true
          | _ -> false)
        >>= fun a -> return (`Address a) <?> "tezos-address" in
      let get_nat = function `Nat n -> n | _ -> assert false in
      let get_address = function `Address a -> a | _ -> assert false in
      let parser =
        let whitespace_p = function ' ' | '\t' | '\n' -> true | _ -> false in
        let skip_wp = skip_while whitespace_p in
        let make_field name type_parser =
          string_ci name *> skip_wp *> char ':' *> skip_wp *> type_parser
          >>= fun v -> return (name, v) in
        let choices =
          let rec go : type a. a record -> _ t list = function
            | Nil -> []
            | Nat {field; right} -> make_field field natural :: go right
            | Address {field; right} -> make_field field address :: go right
            | Address_option {field; right} ->
                make_field field address :: go right in
          go r in
        skip_wp
        *> sep_by1 skip_wp (choice ~failure_msg:"choice between fields" choices)
        <* skip_wp <* end_of_input
        >>= fun l ->
        let one f =
          List.filter_map l ~f:(fun (k, v) ->
              if String.equal f k then Some v else None)
          |> function
          | [] -> Fmt.failwith "Missing field: %S" f
          | [one] -> one
          | _more -> Fmt.failwith "Too many %S fields" f in
        dbgf "l: %s" (String.concat ~sep:"," (List.map l ~f:fst)) ;
        let rec make_result : type a. a record -> a = function
          | Nil -> ()
          | Nat {field; right} -> (get_nat (one field), make_result right)
          | Address {field; right} ->
              (get_address (one field), make_result right)
          | Address_option {field; right} ->
              ( (try Some (get_address (one field)) with _ -> None)
              , make_result right ) in
        try return (make_result r) with Failure f -> fail f in
      match parse_string parser s with
      | Ok o -> Ok (f o)
      | Error s -> Error (`Msg s)

    let transfer_item =
      address_option_field "from"
      @@ address_field "to" @@ nat_field "amount" @@ nat_field "token" Nil

    let parse_transfer s =
      parse_record transfer_item s ~f:(fun (f, (t, (a, (k, ())))) ->
          `Transfer_item (f, t, a, k))

    let update_operators_item =
      address_field "owner" @@ address_field "operator" @@ nat_field "token"
      @@ Nil

    let parse_update_operators_item s =
      parse_record update_operators_item s ~f:(fun (ow, (op, (tok, ()))) ->
          (ow, op, tok))

    let test_this () =
      dbgf "TTT: test" ;
      let test s =
        dbgf "trying: %s" s ;
        match parse_transfer s with
        | Ok (`Transfer_item (fro, t, amo, k)) ->
            dbgf "ok %d %ds %a → %s" amo k Fmt.(Dump.option string) fro t
        | Error (`Msg s) -> dbgf "Error: %s" s in
      test
        "from: tz1lkjdeid32NNFr amount: 10002 to : KT1lijde3098FR token-id: 3" ;
      test "   amount: 210_200  from:  tz1lkjdeid32NNFr   amount: 10002 " ;
      test "   amount: 210_200 wut from:  tz1lkjdeid32NNFr   amount: 10002" ;
      test "   amount: 210_200  from :  tz1lkjdeid32NNFr  " ;
      ()

    let test_that () =
      dbgf "TTT: test" ;
      let test s =
        dbgf "trying: %s" s ;
        match parse_update_operators_item s with
        | Ok (ow, op, tok) -> dbgf "ok ow %s → %s tok: %d" ow op tok
        | Error (`Msg s) -> dbgf "Error: %s" s in
      test "owner: toihde2oihd33 operator : deldsije " ;
      test "   owner: odkedse operator: deji  amount: 10002 " ;
      ()
  end

  let transfer_converter () =
    (* Little experiment in cmdliner custom converters: *)
    let open Cmdliner.Arg in
    let printer ppf (`Transfer_item (f, t, a, o)) =
      Fmt.pf ppf "[from: %s] to: %s amount: %d token: %d"
        (Option.value f ~default:"optional-from")
        t a o in
    conv ~docv:"call" (Parse_records.parse_transfer, printer)

  let update_operators_item_converter () =
    (* Little experiment in cmdliner custom converters: *)
    let open Cmdliner.Arg in
    let printer ppf (ow, op, tok) =
      Fmt.pf ppf "owner: %s operator: %s token: %d" ow op tok in
    conv ~docv:"call" (Parse_records.parse_update_operators_item, printer)

  let commands () =
    List.concat
      [ print_and_call_cmds () ~entry_point:"transfer"
          ~argument:
            Cmdliner.(
              Term.(
                let conv_tr = transfer_converter () in
                let doc =
                  let pp = Arg.conv_printer conv_tr in
                  Fmt.str "Transfer element, example: '%a'" pp
                    (`Transfer_item (Some "tz1from", "tz2destination", 42, 1))
                in
                const (fun l -> `Batch_transfer l)
                $ Arg.(
                    value
                      (pos_all conv_tr
                         (* (t4 ~sep:':' string string int int) *)
                         []
                         (info [] ~doc ~docv:"TRANSFER-ITEM")))))
      ; print_and_call_cmds () ~entry_point:"mint"
          ~argument:
            Cmdliner.(
              Term.(
                const (fun addr amount id symbol ->
                    `Mint (addr, amount, id, symbol))
                $ Arg.(
                    required
                      (pos 0 (some string) None
                         (info [] ~docv:"ADDRESS"
                            ~doc:"The address to mint to.")))
                $ Arg.(
                    required
                      (pos 1 (some int) None
                         (info [] ~docv:"AMOUNT"
                            ~doc:"The amount of tokens to mint.")))
                $ Arg.(
                    value
                      (opt int 0
                         (info ["token-id"] ~docv:"NAT" ~doc:"The token ID.")))
                $ Arg.(
                    value
                      (opt string "XF2"
                         (info ["token-symbol"] ~docv:"STRING"
                            ~doc:"The token symbol.")))))
      ; print_and_call_cmds () ~entry_point:"update_operators"
          ~argument:
            Cmdliner.(
              Term.(
                let arg =
                  Arg.(
                    value
                      (pos_all
                         (t2 ~sep:'@'
                            (enum
                               [ ("add", `Add); ("remove", `Remove)
                               ; ("rm", `Remove) ])
                            (update_operators_item_converter ())
                            (* (list ~sep:',' int) *))
                         []
                         (info []
                            ~doc:
                              (Fmt.str "Make a sub-operation (add|rm|remove)")
                            ~docv:
                              "WHAT@ operator:OPERATOR owner:OWNER \
                               token:TOKEN-ID"))) in
                const (fun l -> `Update_operators l) $ arg))
      ; [ cmd_print_expression () ~entry_point:"initialization"
            ~end_doc_sentence:"of the storage"
            ~argument:
              Cmdliner.(
                Term.(
                  const (fun a -> `Init_storage a)
                  $ Arg.(
                      required
                        (pos 0 (some string) None
                           (info [] ~docv:"ADDRESS"
                              ~doc:"The contract Administrator."))))) ] ]
end

module Storage_queries = struct
  module Fa2 = Fa2_interface_mutran_contract

  let get_state ?(known_owners = []) ?(known_operators = []) state client
      ~address () =
    Client.get_storage_json state client ~address
    >>= fun json ->
    Fa2.Storage.of_json json
    >>= fun storage ->
    Client.get_rpc_json state client
      ~path:
        (Fmt.str "/chains/main/blocks/head/context/contracts/%s/balance"
           address)
    >>= fun json_balance ->
    Io.catch (fun () ->
        match json_balance with
        | `String s -> Int.of_string s
        | _ -> assert false)
    >>= fun bal ->
    let tokens_bm =
      match storage.tokens with
      | Fa2.M_big_map.List _ -> assert false
      | Fa2.M_big_map.Int n -> n in
    let ledger_bm =
      match storage.ledger with
      | Fa2.M_big_map.List _ -> assert false
      | Fa2.M_big_map.Int n -> n in
    let operators_set_bm =
      match storage.operators with
      | Fa2.M_big_map.List _ -> assert false
      | Fa2.M_big_map.Int n -> n in
    let last_token =
      match storage.all_tokens with
      | Fa2.M_nat.Int i -> i
      | Fa2.M_nat.Big_int bi -> Big_int.int_of_big_int bi in
    let all_tokens = List.init last_token ~f:Fn.id in
    let big_map_value state client ~big_map_id ~concrete_data ~michelson_type =
      Client.command state client
        ["hash"; "data"; concrete_data; "of"; "type"; michelson_type]
      >>= fun res ->
      let hash =
        Command.Command_result.get_stdout state res
        |> String.split ~on:'\n' |> List.map ~f:String.strip
        |> List.find_map ~f:(fun l ->
               match String.split ~on:' ' l with
               | "Script-expression-ID-Hash:" :: hash :: _ -> Some hash
               | _ -> None) in
      match hash with
      | None ->
          Console.warnf "Could not create big-map key for %s" concrete_data ;
          Io.return None
      | Some hash ->
          let path =
            "/chains/main/blocks/head/context/big_maps"
            // Int.to_string big_map_id // hash in
          Io.dispatch
            (Client.get_rpc_json state client ~path)
            ~ok:(fun json -> Io.return (Some json))
            ~error:(fun _ -> Io.return None) in
    List.fold all_tokens ~init:(Io.return []) ~f:(fun prevm i ->
        prevm
        >>= fun prev ->
        big_map_value state client ~concrete_data:(Int.to_string i)
          ~michelson_type:"nat" ~big_map_id:tokens_bm
        >>= fun json_opt ->
        match json_opt with
        | None ->
            Console.warnf "Could not create big-map key for %d" i ;
            Io.return prev
        | Some json ->
            Fa2.Tokens_element.of_json json
            >>= fun token ->
            dbgf "tok %d: %a" i Fa2.Tokens_element.pp token ;
            Io.return ((i, token) :: prev))
    >>= fun tokens ->
    List.fold known_owners ~init:(Io.return []) ~f:(fun prevm addr ->
        prevm
        >>= fun prev ->
        List.fold all_tokens ~init:(Io.return []) ~f:(fun prevm i ->
            prevm
            >>= fun prev ->
            big_map_value state client
              ~concrete_data:(Fmt.str "Pair %S %d" addr i)
              ~michelson_type:"pair address nat" ~big_map_id:ledger_bm
            >>= function
            | None ->
                (* Console.warnf "Could not get big-map key for %s,%d" addr i ; *)
                Io.return prev
            | Some json -> (
                Fa2.M_nat.of_json json
                >>= function
                | nat ->
                    let elt_int =
                      match nat with
                      | Fa2.M_nat.Int elt -> elt
                      | Fa2.M_nat.Big_int bi -> Big_int.int_of_big_int bi in
                    Io.return ((i, elt_int) :: prev) ))
        >>| List.rev
        >>= fun bals ->
        List.fold known_operators ~init:(Io.return []) ~f:(fun prevm op ->
            prevm
            >>= fun prev ->
            List.fold all_tokens ~init:(Io.return []) ~f:(fun prevm tok ->
                prevm
                >>= fun prev ->
                big_map_value state client
                  ~concrete_data:(Fmt.str "Pair %S (Pair %S %d)" addr op tok)
                  ~michelson_type:"pair address (pair address nat)"
                  ~big_map_id:operators_set_bm
                >>= function
                | None -> Io.return prev
                | Some _ (* Unit *) -> Io.return (tok :: prev))
            >>= function
            | [] -> Io.return prev
            | founds -> Io.return ((op, List.rev founds) :: prev))
        >>| List.rev
        >>= fun ops -> Io.return ((addr, bals, ops) :: prev))
    >>= fun accounts ->
    Io.return (bal, storage, List.rev tokens, List.rev accounts)

  let show_state ?(output = `Human) state client ~address ~known_owners
      ~known_operators () =
    Client.init state client
    >>= fun () ->
    get_state ~known_owners ~known_operators state client ~address ()
    >>= fun (balance, storage, tokens, accounts) ->
    let open Fa2 in
    match output with
    | `Human ->
        let pp_address = Fmt.(of_to_string M_address.to_concrete) in
        Console.say
          Fmt.(
            let desc k v =
              box ~indent:2 (const string k ++ const string ":" ++ sp ++ v)
            in
            let self : _ t = fun ppf f -> f ppf () in
            let big_map = function
              | M_big_map.List _ -> const string "ERROR???"
              | M_big_map.Int n -> const int n in
            let m_string str ppf = function
              | M_string.Raw_string s -> str ppf s
              | M_string.Raw_hex_bytes s -> str ppf s in
            let pp_token_name ppf n =
              match
                List.find_map tokens ~f:(function
                  | i, tokelt when i = n -> Some tokelt.symbol
                  | _ -> None)
              with
              | Some s -> pf ppf "%a(%d)" (m_string string) s n
              | None -> pf ppf "Token-%d" n in
            let pp_token ppf (n, tok) =
              let open Tokens_element in
              pf ppf "%d = %a." n (m_string string) tok.symbol in
            let pp_account ppf (addr, bals, ops) =
              (* let open Ledger_element in *)
              let pp_balances =
                list ~sep:cut (fun ppf (tok, bal) ->
                    pf ppf "- Balance: %d %a" bal pp_token_name tok) in
              let pp_operators =
                list ~sep:cut (fun ppf (addr, toks) ->
                    pf ppf "- Operator: %S -> [%a]" addr
                      (list ~sep:(any ", ") int)
                      toks) in
              let f =
                box (* ~indent:2 *)
                  ( kstr (const string) "Owner: %S" addr
                  ++ sp
                  ++ ( match ops with
                     | [] -> const string "[0 ops]"
                     | more -> vbox (const pp_operators more) )
                  ++ sp
                  ++
                  match bals with
                  | [] -> const string "[0 toks]"
                  | more -> vbox (const pp_balances more) ) in
              f ppf () in
            vbox ~indent:2
              (const (list ~sep:cut self)
                 [ desc "Contract" (const string address)
                 ; desc "Balance" (kstr (const string) "%d mutez" balance)
                 ; desc "Administrator" (const pp_address storage.administrator)
                 ; desc "Status"
                     (const string
                        M_bool.(
                          match storage.paused with
                          | True -> "PAUSED"
                          | False -> "Ready"))
                 ; desc "Tokens-big-map" (big_map storage.tokens)
                 ; desc "Ledger-big-map" (big_map storage.ledger)
                 ; desc "Operators-big-map" (big_map storage.operators)
                 ; desc "All-Tokens"
                     ( match storage.all_tokens with
                     | M_nat.Int 0 -> const string "None"
                     | M_nat.Int _ | M_nat.Big_int _ ->
                         vbox ~indent:0 (const (list ~sep:cut pp_token) tokens)
                     )
                 ; desc "Known-Owners-and-Operators"
                     ( match accounts with
                     | [] -> const string "None"
                     | more ->
                         vbox ~indent:0
                           (const
                              (list ~sep:cut (const string "* " ++ pp_account))
                              more) ) ])) ;
        Io.return ()

  let cmd_show_storage () =
    let open Cmdliner in
    let open Term in
    ( const
        (fun state
             client
             address
             known_owners
             known_operators
             known_address
             ()
             ->
          let known_owners = known_owners @ known_address in
          let known_operators = known_operators @ known_address in
          Io.run
            (show_state state client ~address ~known_owners ~known_operators))
      $ State.cmdliner_term ()
      $ Client.uri_cmdliner_term ()
      $ Arg.(
          required
            (pos 0 (some string) None
               (info [] ~doc:"Contract address" ~docv:"KT1-ADDRESS")))
      $ Arg.(
          value
            (opt_all string []
               (info ["owner"] ~docv:"ADDRESS" ~doc:"Get account for $(docv).")))
      $ Arg.(
          value
            (opt_all string []
               (info ["operator"] ~docv:"ADDRESS"
                  ~doc:"Register $(docv) as a potential operator.")))
      $ Arg.(
          value
            (opt_all string []
               (info ["known-address"] ~docv:"ADDRESS"
                  ~doc:"Register $(docv) as a potential owner OR operator.")))
      $ const ()
    , info "show-storage" ~doc:"Show the storage of a contract" )

  let cmds () = [cmd_show_storage ()]
end

let cmd_originate_contract () =
  let run ?output_address state client ~contracts ~administrator () =
    Client.init state client
    >>= fun () ->
    Client.command state client ["bootstrapped"]
    >>= fun bootstrapped ->
    dbgf "Bootstrapped: %s"
      (Command.Command_result.get_stdout state bootstrapped) ;
    let results =
      List.map contracts ~f:(fun (m, i) ->
          let module Contract = (val m : Sigs.CONTRACT) in
          let module Interface = (val i : Sigs.CONTRACT_INTERFACE) in
          let code = Contract.code in
          let arg = Contract.init ~admin:administrator in
          let name = Contract.name in
          dbgf "Originating %s" name ;
          Client.originate state client ~arg ~name (`Literal code)
          >>= fun address ->
          dbgf "Originated %s -> %s" name address ;
          Io.return (name, Contract.description, address)) in
    Io.return results
    >>= function
    | [] ->
        Console.warn Fmt.(const text "Originated 0 contracts...") ;
        Io.return ()
    | res ->
        Console.say
          Fmt.(
            let one_result ppf = function
              | Ok (name, description, address) ->
                  box
                    (kstr (const text) "Success: %s (%s) -> %s" name description
                       address)
                    ppf ()
              | Error (`System (m, _)) ->
                  box
                    (styled `Red (const string "ERROR:") ++ sp ++ const text m)
                    ppf () in
            let all_results ppf res =
              List.iter res ~f:(fun one ->
                  cut ppf () ;
                  pf ppf "* %a" one_result one) in
            vbox ~indent:2 (const text "Originations:" ++ const all_results res)) ;
        Option.iter output_address ~f:(fun outpath ->
            let addresses =
              List.concat_map res ~f:(function Ok (_, _, a) -> [a] | _ -> [])
            in
            Stdio.Out_channel.write_lines outpath addresses) ;
        if List.for_all res ~f:(function Ok _ -> true | _ -> false) then
          Io.return ()
        else (
          Console.errorf "There were errors, see %S" state.State.root_path ;
          Stdlib.exit 5 ) in
  let default_admin = "tz1LG61jvow6t3ZXnNAxxSET49YY9Q6D13BX" in
  let open Cmdliner in
  let open Term in
  ( const (fun state client administrator contracts output_address () ->
        Io.run (run state ?output_address client ~administrator ~contracts))
    $ State.cmdliner_term ()
    $ Client.uri_cmdliner_term ()
    $ Arg.(
        value
          (opt string default_admin
             (info ["administrator"] ~docv:"TEZOS-ADDRESS"
                ~doc:"Address of the administrator.")))
    $ Command_line_helpers.all_or_a_few_contracts_pos_all_term ()
    $ Arg.(
        value
          (opt (some string) None
             (info ["output-address"; "A"]
                ~doc:"Output the addresses of the contracts to a file.")))
    $ const ()
  , info "originate" ~doc:"Originate a given build of FA2-Smartpy." )

let cmd_tezos_client () =
  let open Cmdliner in
  let open Term in
  ( ( const (fun state client with_potential_bake cmd ->
          Io.run (fun () ->
              Client.init state client
              >>= fun () ->
              Client.command state client
                ("--wait" :: Client.wait_arg client :: cmd)
              >>= fun res ->
              let outpath = Command.Command_result.output_path state res in
              Console.say
                Fmt.(kstr (const text) "See output path: `%s`" outpath) ;
              Fmt.pr "%s\n%!" (Command.Command_result.get_stdout state res) ;
              ( if with_potential_bake then (
                Console.say Fmt.(const text "Baking a block") ;
                Client.potential_bake state client )
              else Io.return () )
              >>= fun () -> Io.return ()))
    $ State.cmdliner_term ()
    $ Client.uri_cmdliner_term ()
    $ Arg.(value (flag (info ["with-potential-bake"])))
    $ Arg.(
        value
          (pos_all string []
             (info [] ~docv:"ARGS" ~doc:"The arguments to pass to the client")))
    )
  , info "client" ~doc:"Run a command with a configured client" )

let cmd_fund_address () =
  let open Cmdliner in
  let open Term in
  ( ( const (fun state client address amount ->
          Io.run (fun () ->
              Client.init state client
              >>= fun () ->
              Client.raw_transfer state client ~amount ~dst:address
                ~src:client.Client.funder
              >>= fun _ ->
              Client.potential_bake state client
              >>= fun () ->
              Client.get_balance state client ~address
              >>= fun bal ->
              Console.say
                Fmt.(
                  kstr (const text) "Balance for %s is now %d mutez." address
                    bal) ;
              Io.return ()))
    $ State.cmdliner_term ()
    $ Client.uri_cmdliner_term ()
    $ Arg.(required (pos 0 (some string) None (info [] ~docv:"ADDRESS")))
    $ Arg.(required (pos 1 (some int) None (info [] ~docv:"MUTEZ-AMOUNT"))) )
  , info "fund-address"
      ~doc:"Transfer mutez from the configured \"funding authority\"" )

module Make_implementation_document = struct
  let cmd () =
    let open Cmdliner in
    let open Term in
    ( ( const (fun file outfile ->
            Io.run (fun () ->
                let lines = Stdio.In_channel.read_lines file in
                let data =
                  let end_state, rev_lines =
                    List.foldi
                      ~init:(`Python, [])
                      lines
                      ~f:(fun ith (state, acc) line ->
                        match
                          String.chop_prefix_exn (String.strip line)
                            ~prefix:"##"
                        with
                        | in_markdown -> (
                            let markdown =
                              match
                                String.chop_prefix_exn ~prefix:" " in_markdown
                              with
                              | s -> s
                              | exception _ -> in_markdown in
                            match (state, acc) with
                            | `Python, [] -> (`Markdown, [markdown])
                            | `Python, more ->
                                (`Markdown, markdown :: "" :: "```" :: more)
                            | `Markdown, acc -> (`Markdown, markdown :: acc) )
                        | exception _ -> (
                            let start_python =
                              Fmt.str
                                "```{.python .numberLines startFrom=\"%d\"}"
                                (ith + 1) in
                            match (state, acc) with
                            | `Python, [] -> (`Python, [line; start_python])
                            | `Python, acc -> (`Python, line :: acc)
                            | `Markdown, acc ->
                                (`Python, line :: start_python :: "" :: acc) ))
                  in
                  let rev_result =
                    match end_state with
                    | `Python -> "" :: "```" :: rev_lines
                    | `Markdown -> rev_lines in
                  let header =
                    ["---"; Fmt.str "title: Implementation `%s`" file; "---"]
                  in
                  header @ [""; ""] @ List.rev rev_result
                  |> String.concat ~sep:"\n" in
                Stdio.Out_channel.write_all outfile ~data ;
                Io.return ()))
      $ Arg.(
          required (pos 0 (some string) None (info [] ~doc:"Input python file")))
      $ Arg.(
          required
            (pos 1 (some string) None (info [] ~doc:"Output markdown file"))) )
    , info "make-implementation-document"
        ~doc:"Transform python file into markdown document." )
end

let () =
  let open Cmdliner in
  let version = "0.0.0-dev" in
  let appname = Path.basename Caml.Sys.argv.(0) in
  let help = Term.(ret (pure (`Help (`Auto, None))), info appname ~version) in
  Term.exit
    (Term.eval_choice help
       ( [ cmd_self_test (); cmd_list_contract_variants (); cmd_print_code ()
         ; cmd_originate_contract (); cmd_tezos_client (); Benchmark.cmd ()
         ; cmd_fund_address (); Crypto.Account.account_of_phrase_cmd ()
         ; Tutorial_generator.cmd (); Client.show_uri_doc_cmd ()
         ; Make_implementation_document.cmd () ]
       @ Make_expressions.commands ()
       @ Storage_queries.cmds () ))
